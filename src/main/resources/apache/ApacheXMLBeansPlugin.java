package plugins.stef.library.apache;

import icy.plugin.abstract_.Plugin;
import icy.plugin.interface_.PluginLibrary;

/**
 * Icy wrapper for the Apache XMLBeans library.
 * 
 * @author Stephane Dallongeville
 */
public class ApacheXMLBeansPlugin extends Plugin implements PluginLibrary
{
    //
}
